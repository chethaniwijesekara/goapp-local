package inventoryrollforwardservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	"fmt"
	"github.com/bojand/ghz/runner"

	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"time"
	"../report_generation"
)

var sourceFlightR int32
var dateRangeStartingDateR string
var dateRangeSizeR int
var flightNumbersR[] string


func rollforwardDataFunction(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	rollforwardRQ := &aeroinventory.RollforwardRQ{}

	rollforwardRQ.SourceFlightId = int64(sourceFlightR)

	flightSearchCriteria := aeroinventory.FlightSearchCriteriaForRollforward{}
	flightSearchCriteria.AircraftModel = "A320-168Y"
	flightSearchCriteria.FlightNumbers = flightNumbersR
	flightSearchCriteria.Sectors = []string{"AE_SHJ_SHJ/LK_CMB_CMB"}
	flightSearchCriteria.FlightStatus = []string{"ACT"}

	frequency := aeroinventory.Frequency{}
	frequency.Fri = true
	frequency.Mon = true
	frequency.Sun = true
	frequency.Tue = true
	frequency. Wed = true
	frequency.Thu = true
	frequency.Sat = true

	flightSearchCriteria.Frequency = &frequency

	timeOfDay := aeroinventory.TimeOfDayOption{}
	timeOfDay.OptionName = "All Day"
	timeOfDay.OptionStartTime = "00:00"
	timeOfDay.OptionEndTime = "23:59"

	flightSearchCriteria.TimeOfDays = []*aeroinventory.TimeOfDayOption{&timeOfDay}

	rollforwardOptions := aeroinventory.RollforwardOption{}
	rollforwardOptions.AllocationOption = "OVERRIDE_ALL"
	rollforwardOptions.CapacityOptionForCurtail = "IGNORE"
	rollforwardOptions.CapacityOptionForOversell = "IGNORE"

	rollforwardRQ.RollforwardOption = &rollforwardOptions


	selectedInventory := aeroinventory.SelectedInventory{}

	dateRange := aeroinventory.DateRange{}
	layout := "2006-01-02"
	t, err := time.Parse(layout, dateRangeStartingDateR)

	commonDateS := aeroinventory.CommonDate{}
	commonDateS.DateText = dateRangeStartingDateR

	dateRange.FromDate = &commonDateS

	endDate := t.AddDate(0,0, dateRangeSizeR)
	commonDateE := aeroinventory.CommonDate{}
	commonDateE.DateText =endDate.Format(layout)

	dateRange.ToDate = &commonDateE

	selectedInventory.DateRange = &dateRange


	dateRangeStartingDateR = endDate.AddDate(0,0,1).Format(layout)


	bcInvet := aeroinventory.BCSubtypeInventory{}
	bcInvet.BcSubtypeCode = "STANDARD"
	bcInvet.SelectedBookingClassCodes = []string{"C1"}

	ccInvet := aeroinventory.CabinInventory{}
	ccInvet.CabinClassCode = "Y"
	ccInvet.BcSubtypeInventories = []*aeroinventory.BCSubtypeInventory{&bcInvet}


	segmentInvet := aeroinventory.SegmentInventory{}
	segmentInvet.SegmentCode = "SHJ/CMB"
	segmentInvet.CabinInventories = []*aeroinventory.CabinInventory{&ccInvet}

	selectedInventory.SegmentInventories = []*aeroinventory.SegmentInventory{&segmentInvet}

	rollforwardRQ.SelectedInventories = []*aeroinventory.SelectedInventory{&selectedInventory}
	rollforwardRQ.FlightSearchCriteriaForRollforward = &flightSearchCriteria

	fmt.Print(rollforwardRQ.SelectedInventories[0].DateRange.FromDate.DateText+"  "+
		rollforwardRQ.SelectedInventories[0].DateRange.ToDate.DateText+"\n")

	binData, err := proto.Marshal(rollforwardRQ)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return binData
}

func RollForwardInventory(numberOfRequests int,
	numberOfConcurrentRequests uint,
	sourceFlight int32,
	dateRangeStartingDate string,
	dateRangeSize int,
	flightNumbers[] string){

	sourceFlightR = sourceFlight
	dateRangeStartingDateR = dateRangeStartingDate
	dateRangeSizeR = dateRangeSize
	flightNumbersR = flightNumbers

	fmt.Print("Roll Forward Inventory calls with :\n\n")
	report, err := runner.Run(
		"aeroinventory.InventoryRollforwardService.rollforwardInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/inventory_rollforward.proto",
			[]string{}),
		runner.WithInsecure(true),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(rollforwardDataFunction),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	report_generation.PrintReport(report)
}