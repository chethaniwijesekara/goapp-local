package inventoryservice

import (
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"../report_generation"
)

func sellInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {
	sellReq := &aeroinventory.InventorySpaceRequest{}

	sellReq.FlightSegmentInventories= []*aeroinventory.FlightSegmentInventory {GetCancelAndSellReqObject(cd)}
	binData, err := proto.Marshal(sellReq)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func SellInventory(numberOfRequests int,
	numberOfConcurrentRequests uint,
	flightNumbers[] string,
	childCapacities[] string,
	infantCapacities[] string,
	adultCapacities[] string,
	bookingClass string,
	departureDateLocal string)  {

	flightNumbersR = flightNumbers
	bookingClassR = bookingClass
	departureDateLocalR = departureDateLocal
	numberOfRequestsR = numberOfRequests

	numOfReqPerFlightR = 3
	childCapacitiesRN = divideCounts(childCapacities,numOfReqPerFlightR)
	adultCapacitiesRN = divideCounts(adultCapacities,numOfReqPerFlightR)
	infantCapacitiesRN = divideCounts(infantCapacities,numOfReqPerFlightR)

	fmt.Print("Sell Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.InventoryService.sellInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/InventoryService.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(sellInventoryDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	report_generation.PrintReport(report)
}

