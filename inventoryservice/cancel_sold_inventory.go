package inventoryservice

import (
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"../report_generation"
)

func cancelSoldInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {
	cancelReq := &aeroinventory.SoldInventoryCancellationRequest{}

	cancelReq.FlightSegmentInventories= []*aeroinventory.FlightSegmentInventory {GetCancelAndSellReqObject(cd)}
	binData, err := proto.Marshal(cancelReq)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}


func CancelSoldInventory(numberOfRequests int,
	numberOfConcurrentRequests uint,
	flightNumbers[] string,
	childCapacities[] string,
	infantCapacities[] string,
	adultCapacities[] string,
	bookingClass string,
	departureDateLocal string)  {

	flightNumbersR = flightNumbers
	bookingClassR = bookingClass
	departureDateLocalR = departureDateLocal

	numOfReqPerFlightR = 3
	childCapacitiesRN = divideCounts(childCapacities,numOfReqPerFlightR)
	adultCapacitiesRN = divideCounts(adultCapacities,numOfReqPerFlightR)
	infantCapacitiesRN = divideCounts(infantCapacities,numOfReqPerFlightR)

	fmt.Print("Cancel Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.InventoryService.cancelSoldInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/InventoryService.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(cancelSoldInventoryDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)

}