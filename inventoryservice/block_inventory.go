package inventoryservice

import (
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"../report_generation"
)

var timePeriodR string
func blockInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {
	blockReq := &aeroinventory.InventoryBlockSpaceRequest{}
	blockReq.FlightSegmentInventories= []*aeroinventory.FlightSegmentInventory {GetCancelAndSellReqObject(cd)}
	blockReq.TimePeriod= timePeriodR

	binData, err := proto.Marshal(blockReq)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}


func BlockInventory(numberOfRequests int,
	numberOfConcurrentRequests uint,
	flightNumbers[] string,
	childCapacities[] string,
	infantCapacities[] string,
	adultCapacities[] string,
	bookingClass string,
	departureDateLocal string,
	timePeriod string)  {

	flightNumbersR = flightNumbers
	bookingClassR = bookingClass
	departureDateLocalR = departureDateLocal
	timePeriodR = timePeriod

	numOfReqPerFlightR = 3
	childCapacitiesRN = divideCounts(childCapacities,numOfReqPerFlightR)
	adultCapacitiesRN = divideCounts(adultCapacities,numOfReqPerFlightR)
	infantCapacitiesRN = divideCounts(infantCapacities,numOfReqPerFlightR)


	fmt.Print("Block Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.InventoryService.blockInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/InventoryService.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(blockInventoryDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)




}