package goshowservice

import (
	"com/accelaero/aeroinventory"
	goShow "com/accelaero/aeroinventory/goshow"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/jhump/protoreflect/desc"
	"google.golang.org/protobuf/proto"
	"os"
	"strconv"
)

var departureDateAndTimeLocalR string
var bookingClassCodeR string
var childCapacityR int32
var infantCapacityR int32
var adultCapacityR int32
var flightNumbersStartsWithR int64
var numberOfFlightsR int64


func goShowDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	req := &goShow.GoShowRQ{}

	FlightNumber := flightNumbersStartsWithR + (cd.RequestNumber % numberOfFlightsR)
	req.FlightNumber = "G9" + strconv.FormatInt(FlightNumber, 10)

	departureDateTimeCommonForFromFlight :=&aeroinventory.CommonDate{}
	departureDateTimeCommonForFromFlight.DateText = departureDateAndTimeLocalR

	req.DepartureDateAndTimeLocal = departureDateTimeCommonForFromFlight
	req.SegmentCode = "SHJ/CMB"
	req.CabinClassCode = "Y"
	req.BookingClassSubtypeCode = 1
	req.BookingClassCode = bookingClassCodeR
	req.Adults = adultCapacityR
	req.Children = childCapacityR
	req.Infants = infantCapacityR

	binData, err := proto.Marshal(req)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func GoShow(numberOfRequests int64,
	flightNumberStartsWith int64,
	departureDateAndTimeLocal string,
	bookingClassCode string,
	adultCapacity int32,
	childCapacity int32,
	infantCapacity int32,
	numberOfFlights int64)  {

	flightNumbersStartsWithR = flightNumberStartsWith
	departureDateAndTimeLocalR  = departureDateAndTimeLocal
	bookingClassCodeR = bookingClassCode
	adultCapacityR = adultCapacity
	childCapacityR = childCapacity
	infantCapacityR = infantCapacity

	numberOfFlightsR = numberOfFlights

	report, err := runner.Run(
		"aeroinventory.goshow.GoShowService.GoShow",
		"localhost:6565",
		runner.WithInsecure(true),
		runner.WithConcurrency(1),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(goShowDataFunc),
		runner.WithProtoFile(
			"/home/chethan/Codebase/goapp-local/goProtos/proto/go_show.proto",
			[]string{}),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}

	printer.Print("pretty")

}
