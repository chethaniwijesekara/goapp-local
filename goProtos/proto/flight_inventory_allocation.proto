syntax = "proto3";

package aeroinventory;

option java_multiple_files = true;

import "google/api/annotations.proto";
import "commons.proto";

option java_package = "com.accelaero.aeroinventory";
option go_package = "com/accelaero/aeroinventory";

message FlightCCSegBCInventoryRS {
    int32 fccSegBCInventoryId = 1;
    int32 fccSegInventoryId = 2;
    string bookingClassCode = 3;
    int32 allocatedSeats = 4;
    int32 availableSeats = 5;
    int32 soldSeats = 6;
    int32 ohdSeats = 7;
    int32 blockedSeats = 8;
    int32 acquiredSeats = 9;
    int32 cancelledSeats = 10;
    bool priority = 11;
    bool closed = 12;
    string statusChangeAction = 13;
    int32 rank = 14;
    int32 oneWayP2PCount = 15;
    int32 returnP2PCount = 16;
    int32 oneWayConnP2PCount = 17;
    int32 returnConnP2PCount = 18;
    int32 soldInCount = 19;
    int32 soldOutCount = 20;
    int32 onholdInCount = 21;
    int32 onholdOutCount = 22;
    string fareRules = 23;
    int32 agentCount = 24;
    int32 releaseTimeMins = 25;
    string bookingClassName = 26;
    string nestGroup = 27;
    int32 nestedAvailability = 28;
}

message FlightCCSegInventoryRS {
    int32 fccSegInventoryId = 1;
    int32 flightSegId = 2;
    int32 fccInventoryId = 3;
    string cabinClassCode = 4;
    int32 adultCapacity = 5;
    int32 infantCapacity = 6;
    string curtailOversellFlag = 7;
    int32 curtailOversellCount = 8;
    int32 adultSoldCount = 9;
    int32 adultOHDCount = 10;
    int32 adultBlockedCount = 11;
    int32 adultAvailCount = 12;
    int32 infantSoldCount = 13;
    int32 infantOHDCount = 14;
    int32 infantBlockedCount = 15;
    int32 infantAvailCount = 16;
    int32 adultStandbySoldCount = 17;
    int32 adultStandbyOHDCount = 18;
    int32 adultStandbyBlockedCount = 19;
    int32 infantStandbySoldCount = 20;
    int32 infantStandbyOHDCount = 21;
    int32 infantStandbyBlockedCount = 22;
    repeated FlightCCSegBCInventoryRS standardInventories = 23;
    repeated FlightCCSegBCInventoryRS nonStandardInventories = 24;
    repeated FlightCCSegBCInventoryRS fixedInventories = 25;
    repeated FlightCCSegBCInventoryRS standByInventories = 26;
}

message FlightSegInventoryRS {
    repeated FlightCCSegInventoryRS fccSegInventories = 1;
    int32 flightSegId = 2;
    string segmentCode = 3;
    int32 segmentSequence = 4;
}

message SegmentWiseUtilizedSeats {
    string segmentCode = 1;
    repeated CabinWiseUtilizedSeats cabinWiseUtilizedSeats = 2;
}

message CabinWiseUtilizedSeats {
    string cabinClassCode = 1;
    int32 overlappingFixedCount = 2;
    int32 nonFixedAvailability = 3;
    int32 soldAdultChildSeatCount = 4;
    int32 soldInfantSeatCount = 5;
}

//message OverlappingFlightSegWiseUtilizedSeats {
//    int32 overlappingFlightId = 1;
//    repeated SegmentWiseUtilizedSeats segmentWiseUtilizedSeats = 2;
//}

message FlightInventoryRS {
    repeated FlightSegInventoryRS flightSegments = 1;
    int32 flightId = 2;
    string flightNumber = 3;
    string status = 4;
    string inventoryStatus = 5;
    string departureDateAndTimeZulu = 6;
    string departureDateAndTimeLocal = 7;
    string arrivalDateAndTimeZulu = 8;
    string arrivalDateAndTimeLocal = 9;
    string aircraftModel = 10;
    int32 noOfDaysToDeparture = 11;
    string origin = 12;
    string destination = 13;
    repeated string viaAirportCodes = 14;
    //    OverlappingFlightSegWiseUtilizedSeats overlappingFlightSegWiseUtilizedSeats = 15;
    repeated SegmentWiseUtilizedSeats segmentWiseUtilizedSeats = 15;
    bool manuallyClosed = 16;
}

message FlightCCSegBCInventoryRQ {
    int32 fccSegBCInventoryId = 1;
    string bookingClassCode = 2;
    int32 allocatedSeats = 3;
    bool priority = 4;
    bool closed = 5;
}

message FlightCCSegInventoryRQ {
    repeated FlightCCSegBCInventoryRQ fccSegBCInventories = 1;
    int32 fccSegInventoryId = 2;
    string curtailOversellFlag = 3;
    int32 curtailOversellCount = 4;
}

message FlightSegInventoryRQ {
    repeated FlightCCSegInventoryRQ fccSegInventories = 1;
    int32 flightSegId = 2;
}

message FlightInventoryUpdateRQ {
    repeated FlightSegInventoryRQ flightSegments = 1;
    int32 flightId = 2;
    int32 jobId = 3;
}

message FlightInventoryUpdateRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    CommonResponse commonResponse = 3;
}

message FlightInventorySearchRQ {
    int32 flightId = 3;
}

message FlightInventorySearchRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message CloseFlightRQ {
    int32 flightId = 1;
    int32 jobId = 2;
}

message OpenFlightRQ {
    int32 flightId = 1;
    int32 jobId = 2;
}
message CloseFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message OpenFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}
message CloseSelectedBcInFlightRQ {
    int32 flightId = 1;
    repeated string bookingClassList = 2;
    int32 jobId = 3;
}

message CloseSelectedBcInFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message OpenSelectedBcInFlightRQ {
    int32 flightId = 1;
    repeated string bookingClassList = 2;
    int32 jobId = 3;
}

message OpenSelectedBcInFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message SetPriorityInSelectedBcInFlightRQ {
    int32 flightId = 1;
    repeated string bookingClassList = 2;
    int32 jobId = 3;
}

message SetPriorityInSelectedBcInFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message UnsetPriorityInSelectedBcInFlightRQ {
    int32 flightId = 1;
    repeated string bookingClassList = 2;
    int32 jobId = 3;
}

message UnsetPriorityInSelectedBcInFlightRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    FlightInventoryRS flightInventory = 3;
}

message UpdateSelectedFlightInventoryRQ {
    int32 flightId = 1;
    repeated BookingClassWiseInventory bookingClassWiseInventory = 2;
    int32 jobId = 3;
}

message UpdateSelectedFlightInventoryRS {
    ServiceError error = 1;
    repeated ServiceWarning warnings = 2;
    CommonResponse commonResponse = 3;
}


service FlightInventoryService {
    rpc updateFlightInventory (FlightInventoryUpdateRQ) returns (FlightInventoryUpdateRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/update"
            body: "*"
        };
    }

    rpc searchFlightInventory (FlightInventorySearchRQ) returns (FlightInventorySearchRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/search"
            body: "*"
        };
    }
    rpc closeFlight (CloseFlightRQ) returns (CloseFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/closeFlight"
            body: "*"
        };
    }
    rpc openFlight (OpenFlightRQ) returns (OpenFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/openFlight"
            body: "*"
        };
    }
    rpc closeSelectedBcInFlight (CloseSelectedBcInFlightRQ) returns (CloseSelectedBcInFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/closeSelectedBcInFlight"
            body: "*"
        };
    }

    rpc openSelectedBcInFlight (OpenSelectedBcInFlightRQ) returns (OpenSelectedBcInFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/openSelectedBcInFlight"
            body: "*"
        };
    }

    rpc updateSelectedFlightInventory(UpdateSelectedFlightInventoryRQ) returns (UpdateSelectedFlightInventoryRS){
        option (google.api.http) = {
            post: "/aeroinventory/inventory/updateSelectedFlightInventory"
            body: "*"
        };
    }

    rpc setPriorityInSelectedBcInFlight (SetPriorityInSelectedBcInFlightRQ) returns (SetPriorityInSelectedBcInFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/setPriorityInSelectedBcInFlight"
            body: "*"
        };
    }
    rpc unsetPriorityInSelectedBcInFlight (UnsetPriorityInSelectedBcInFlightRQ) returns (UnsetPriorityInSelectedBcInFlightRS) {
        option (google.api.http) = {
            post: "/aeroinventory/inventory/unsetPriorityInSelectedBcInFlight"
            body: "*"
        };
    }
}
