package main

import (
	"fmt"
	"strings"
	"../flightinventoryservice"
)

func InvokeFlightInventoryService(){

	var api string
	fmt.Println("Enter the api to be tested in Inventory Service :\n search flight Inventory API--> 1\n Update Flight Inventory API--> 2")
	fmt.Scanln(&api)

	//search flight Inventory
	if api == "1"{

		var numberOfRequests int
		var numberOfConcurrentRequests uint
		var sourceFlightId string

		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequests)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&numberOfConcurrentRequests)

		fmt.Println("Enter the source flight ids separated by comma : ")
		fmt.Scanln(&sourceFlightId)

		flightinventoryservice.SearchFlightInventory(numberOfRequests, numberOfConcurrentRequests, strings.Split(sourceFlightId, ","))

		//Update Flight Inventory
	} else if api == "2"{

		var numberOfRequests int
		var numberOfConcurrentRequests uint
		var sourceFlightId string
		var flightSegId string
		var fccSegInventoryId string
		var fccSegBCInventoryId string
		var allocatedSeats string
		var priority string
		var closed string

		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequests)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&numberOfConcurrentRequests)

		fmt.Println("Enter the source flight ids separated by comma : ")
		fmt.Scanln(&sourceFlightId)

		fmt.Println("Enter the flight segIds separated by comma : ")
		fmt.Scanln(&flightSegId)

		fmt.Println("Enter the fccSegInventory Ids separated by comma : ")
		fmt.Scanln(&fccSegInventoryId)

		fmt.Println("Enter the fccSegBCInventory Ids separated by comma : ")
		fmt.Scanln(&fccSegBCInventoryId)

		fmt.Println("Enter the source allocated seats separated by comma : ")
		fmt.Scanln(&allocatedSeats)

		fmt.Println("Enter the priority status(true/false) separated by comma : ")
		fmt.Scanln(&priority)

		fmt.Println("Enter the closed Status(true/false) separated by comma : ")
		fmt.Scanln(&closed)

		flightinventoryservice.UpdateFlightInventory(numberOfRequests,
			numberOfConcurrentRequests,
			strings.Split(sourceFlightId, ","),
			strings.Split(flightSegId, ","),
			strings.Split(fccSegInventoryId, ","),
			strings.Split(fccSegBCInventoryId, ","),
			strings.Split(allocatedSeats, ","),
			strings.Split(priority, ","),
			strings.Split(closed, ","))
	}
}
