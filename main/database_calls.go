package main

import (
	"fmt"
	"../databasecalls"
)

func InvokeCleanUpDatabase(){

	var fromDate string
	var toDate string
	var fromFlightNumber string
	var toFlightNumber string

	fmt.Println("Enter the date range starting date (format => yyyy-MM-dd'T'HH:mm:ss) : ")
	fmt.Scanln(&fromDate)

	fmt.Println("Enter the date range ending date: (format => yyyy-MM-dd'T'HH:mm:ss) :")
	fmt.Scanln(&toDate)

	fmt.Println("Enter the flight number range starting flight number: ")
	fmt.Scanln(&fromFlightNumber)

	fmt.Println("Enter the flight number range ending flight number: ")
	fmt.Scanln(&toFlightNumber)

	databasecalls.CleanUpTask(fromDate, toDate, fromFlightNumber, toFlightNumber)
}
