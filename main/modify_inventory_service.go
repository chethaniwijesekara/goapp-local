package main

import (
	"../structs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
	"../modifyinventoryservice"
)

func InvokeModifyInventoryServiceFlow() {

	//read data from data.json
	var modifyInventoryData structs.ModifyInventoryData
	jsonFile, err := os.Open("data/modifyInventoryInput.json")

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened data.json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &modifyInventoryData)

	var api string
	fmt.Println("Enter the api to be tested in Modify Inventory Service :\n   " +
		"Block Inventory & Transform BlockedInventory API --> 1\n   " +
		"Block Inventory & Release Block Inventory--> 2\n   ")
	fmt.Scanln(&api)

	//Roll forward inventory
/*	inventoryrollforwardservice.RollForwardInventory(
		modifyInventoryData.NumberOfRequestsForRF,
		modifyInventoryData.NumberOfConcurrentRequestsForRF,
		modifyInventoryData.SourceFlightId,
		modifyInventoryData.DateRangeStartingDateForRF,
		modifyInventoryData.DateRangeSize,
		modifyInventoryData.FlightNumbers)

	//sleep for a while until RF done
	time.Sleep(2 * time.Minute)

	inventoryservice.SellInventory(
		modifyInventoryData.NumberOfRequestsForSellInventory,
		modifyInventoryData.NumberOfConcurrentRequestsForSellInventory,
		modifyInventoryData.FlightNumbers,
		modifyInventoryData.ChildCapacities,
		modifyInventoryData.InfantCapacities,
		modifyInventoryData.AdultCapacities,
		modifyInventoryData.BookingClass,
		modifyInventoryData.DepartureDateLocalStartsWith)*/

	if api == "1" {
		modifyinventoryservice.ModifyInventoryBlock(
			modifyInventoryData.NumberOfRequestsForModify,
			modifyInventoryData.NumberOfConcurrentRequestsForModify,
			modifyInventoryData.TimePeriod,
			modifyInventoryData.FlightNumbers,
			modifyInventoryData.BookingClass,
			modifyInventoryData.DepartureDateLocalStartsWith,
			modifyInventoryData.ChildCapacities,
			modifyInventoryData.InfantCapacities,
			modifyInventoryData.AdultCapacities)

		time.Sleep(10 * time.Second)

		modifyinventoryservice.ModifyInventoryTransform(
			modifyInventoryData.NumberOfRequestsForTransform,
			modifyInventoryData.NumberOfConcurrentRequestsForTransform)
	}

	if api == "2" {
		modifyinventoryservice.ModifyInventoryBlock(
			modifyInventoryData.NumberOfRequestsForModify,
			modifyInventoryData.NumberOfConcurrentRequestsForModify,
			modifyInventoryData.TimePeriod,
			modifyInventoryData.FlightNumbers,
			modifyInventoryData.BookingClass,
			modifyInventoryData.DepartureDateLocalStartsWith,
			modifyInventoryData.ChildCapacities,
			modifyInventoryData.InfantCapacities,
			modifyInventoryData.AdultCapacities)

		time.Sleep(10 * time.Second)

		modifyinventoryservice.ModifyInventoryReleaseBlocked(
			modifyInventoryData.NumberOfRequestsForTransform,
			modifyInventoryData.NumberOfConcurrentRequestsForTransform)
	}
}
