package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"../structs"
	"../transferinventoryservice"
)

func InvokeTransferInventory() {

	//read data from data.json
	var transferInvetData structs.TransferInvetData
	jsonFile, err := os.Open("data/input_transfer_inventory.json")

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened data.json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &transferInvetData)

	transferinventoryservice.TransferInventory(
		transferInvetData.FromFlightDepartureDateLocalTime,
		transferInvetData.FromFlightNumberStartsWith,
		transferInvetData.NumberOfRequests,
		transferInvetData.ToFlightDepartureDateLocalTime,
		transferInvetData.NumberOfToFlightsPerFlight,
		transferInvetData.BookingClass,
		transferInvetData.ChildSoldCapacity,
		transferInvetData.InfantSoldCapacity,
		transferInvetData.AdultSoldCapacity,
		transferInvetData.ChildOnhCapacity,
		transferInvetData.InfantOnhCapacity,
		transferInvetData.AdultOnhCapacity)

}
