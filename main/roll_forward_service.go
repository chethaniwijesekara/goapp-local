package main

import (
	"fmt"
	"strings"
	"../inventoryrollforwardservice"
)

func InvokeRollForwardService(){

	var api string
	fmt.Println("Enter the api to be tested in Inventory Roll Forward Service: \n   Roll Forward API --> 1")
	fmt.Scanln(&api)

	if api == "1" {

		var numberOfRequests int
		var numberOfConcurrentRequests uint
		var sourceFlightId int32
		var dateRangeStartingDate string
		var dateRangeSize int
		var flightNumbers string

		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequests)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&numberOfConcurrentRequests)

		fmt.Println("Enter the source flight id : ")
		fmt.Scanln(&sourceFlightId)

		fmt.Println("Enter flight numbers : ")
		fmt.Scanln(&flightNumbers)

		fmt.Println("Enter the date range starting date (format => yyyy-MM-dd) : ")
		fmt.Scanln(&dateRangeStartingDate)

		fmt.Println("Enter the date range size : ")
		fmt.Scanln(&dateRangeSize)

		inventoryrollforwardservice.RollForwardInventory(
			numberOfRequests,
			numberOfConcurrentRequests,
			sourceFlightId,
			dateRangeStartingDate,
			dateRangeSize,
			strings.Split(flightNumbers,","))

	}
}
