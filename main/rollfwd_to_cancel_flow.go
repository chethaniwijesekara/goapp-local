package main

import (
	"../inventoryservice"
	"../structs"
	"../inventoryrollforwardservice"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

func InvokeRollFwdToCancelFlow() {

	//read data from data.json
	var rfToCancelData structs.RfToCancelData
	jsonFile, err := os.Open("data/input.json")

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened data.json")
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &rfToCancelData)

	//Roll forward inventory
	inventoryrollforwardservice.RollForwardInventory(
		rfToCancelData.NumberOfRequestsForRF,
		rfToCancelData.NumberOfConcurrentRequestsForRF,
		rfToCancelData.SourceFlightId,
		rfToCancelData.DateRangeStartingDateForRF,
		rfToCancelData.DateRangeSize,
		rfToCancelData.FlightNumbers)

	//sleep for a while untile RF done
	time.Sleep(2 * time.Minute)

	//Block inventory
	inventoryservice.BlockInventory(rfToCancelData.NumberOfRequestsForOnholdSellCancelBlock,
		rfToCancelData.NumberOfConcurrentRequestsForOnholdSellCancelBlock,
		rfToCancelData.FlightNumbers,
		rfToCancelData.ChildCapacities,
		rfToCancelData.InfantCapacities,
		rfToCancelData.AdultCapacities,
		rfToCancelData.BookingClass,
		rfToCancelData.DepartureDateLocalStartsWith,
		rfToCancelData.BlockTimePeriod)

	//Block->Onhold inventory
	inventoryservice.OnholdBlockedInventory(rfToCancelData.NumberOfRequestsForBlockToOnhold,
		rfToCancelData.NumberOfConcurrentRequestsForBlockToOnhold)

	//Onhold->sell inventory
	inventoryservice.SellOnholdInventory(rfToCancelData.NumberOfRequestsForOnholdSellCancelBlock,
		rfToCancelData.NumberOfConcurrentRequestsForOnholdSellCancelBlock,
		rfToCancelData.FlightNumbers,
		rfToCancelData.ChildCapacities,
		rfToCancelData.InfantCapacities,
		rfToCancelData.AdultCapacities,
		rfToCancelData.BookingClass,
		rfToCancelData.DepartureDateLocalStartsWith)

	////Cancel inventory
	inventoryservice.CancelSoldInventory(rfToCancelData.NumberOfRequestsForOnholdSellCancelBlock,
		rfToCancelData.NumberOfConcurrentRequestsForOnholdSellCancelBlock,
		rfToCancelData.FlightNumbers,
		rfToCancelData.ChildCapacities,
		rfToCancelData.InfantCapacities,
		rfToCancelData.AdultCapacities,
		rfToCancelData.BookingClass,
		rfToCancelData.DepartureDateLocalStartsWith)


}
