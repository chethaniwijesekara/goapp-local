package flightservice

import (
	"com/accelaero/aeroinventory"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"strconv"
)

var flightIdG []string
var textG []string
var applyForTargetFlightG []string
var numberOfRequestsG int
var i int

func getFlightNoteSaveDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte{

	var tempNoteDTO []*aeroinventory.NoteDTORQ
	var len = len(flightIdG)

	flightsSaveRQ := &aeroinventory.FlightNoteSaveRQ{}
	noteSaveDTO := aeroinventory.NoteSaveDTO{}

	for i=0; i<len; i++ {
		noteDtoRQ := &aeroinventory.NoteDTORQ{}
		fi, _ := strconv.ParseInt(flightIdG[i], 10, 32)
		noteDtoRQ.FlightId = int32(fi)
		noteDtoRQ.Text = textG[i]
		aft, _ := strconv.ParseBool(applyForTargetFlightG[i])
		noteDtoRQ.ApplyForTargetFlight = aft
		tempNoteDTO = append(tempNoteDTO,noteDtoRQ)
	}

	noteSaveDTO.NoteDto = tempNoteDTO
	flightsSaveRQ.Note = &noteSaveDTO
	binData, err := proto.Marshal(flightsSaveRQ)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData

}

func SaveFlightNote(numberOfRequests int, numberOfConcurrentRequests uint, flightId []string, text []string, applyForTargetFlight []string){

	flightIdG = flightId
	textG = text
	applyForTargetFlightG = applyForTargetFlight
	numberOfRequestsG = numberOfRequests

	report, err := runner.Run(
		"aeroinventory.FlightService.saveFlightNote",
		"localhost:6565",
		runner.WithProtoFile("/home/chethan/Codebase/goapp-local/goProtos/proto/flight.proto", []string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(getFlightNoteSaveDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}
	printer.Print("pretty")

}
