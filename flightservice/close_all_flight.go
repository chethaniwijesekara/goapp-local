package flightservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"time"
)

var dateRangeStartingDateR string
var dateRangeSizeR int

func getCloseAllFlightDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte{
	closeRQ := &aeroinventory.CloseAllFlightRQ{}
	flightSearchCriteria  := aeroinventory.FlightSearchRQ{}
	flightSearchCriteria.Current = 0
//	flightSearchCriteria.FlightNumber = "G9501"
	flightSearchCriteria.PageSize = 10000000
	flightSearchCriteria.FlightStatus = "ACT"
	flightSearchCriteria.FromSeatFactor = 0
	flightSearchCriteria.ToSeatFactor = 999
	flightSearchCriteria.Zulu = false

	frequency := aeroinventory.Frequency{}
	frequency.Fri = true
	frequency.Mon = true
	frequency.Sun = true
	frequency.Tue = true
	frequency. Wed = true
	frequency.Thu = true
	frequency.Sat = true

	flightSearchCriteria.Frequency = &frequency

	layout := "2006-01-02"
	t, err := time.Parse(layout, dateRangeStartingDateR)

	commonDateS := aeroinventory.CommonDate{}
	commonDateS.DateText =dateRangeStartingDateR
	flightSearchCriteria.FromDate = &commonDateS

	endDate := t.AddDate(0,0, dateRangeSizeR)
	commonDateE := aeroinventory.CommonDate{}
	commonDateE.DateText = endDate.Format(layout)
	//commonDateE.DateText = "2023-01-03"
	flightSearchCriteria.ToDate = &commonDateE

	dateRangeStartingDateR = endDate.AddDate(0,0,1).Format(layout)

	closeRQ.FlightSearchCriteria = &flightSearchCriteria

	fmt.Print(flightSearchCriteria.FromDate.DateText+" "+flightSearchCriteria.ToDate.DateText+"\n")
	binData, err := proto.Marshal(closeRQ)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func GetCloseAllFlightsRQ(numberOfRequests int, numberOfConcurrentRequests uint, dateRangeStartingDate string,dateRangeSize int)  {

	dateRangeStartingDateR = dateRangeStartingDate
	dateRangeSizeR = dateRangeSize

	report, err := runner.Run(
		"aeroinventory.FlightService.closeAllFlights",
		"localhost:6565",
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(getCloseAllFlightDataFunc),
		runner.WithProtoFile(
			"/home/chethan/Codebase/goapp-local/goProtos/proto/flight.proto",
			[]string{}),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}

	printer.Print("pretty")

}
