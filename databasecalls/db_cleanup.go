package databasecalls

import (
	aeroinventory "com/accelaero/aeroinventory/loadtesting"
	"database/sql"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	_ "github.com/lib/pq"

	"os"
)

var db *sql.DB

var fromDateG string
var toDateG string
var fromFlightNumberG string
var toFlightNumberG string



func CleanUpTask(fromDate string, toDate string, fromFlightNumber string, toFlightNumber string) {

	fromDateG = fromDate
	toDateG = toDate
	fromFlightNumberG = fromFlightNumber
	toFlightNumberG = toFlightNumber

	config, _ := LoadConfiguration("config.json")

	//get postgresql database connection
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.Database.Host,
		config.Database.Port,
		config.Database.User,
		config.Database.Password,
		config.Database.Dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected!")

	sqlStatement1 := `DELETE FROM aeroinventory.t_roll_fwd_flight;`
	_, err = db.Exec(sqlStatement1)

	sqlStatement2 := `DELETE FROM aeroinventory.t_roll_fwd_sub_batch;`
	_, err = db.Exec(sqlStatement2)

	sqlStatement3 := `DELETE FROM aeroinventory.t_roll_fwd_batch;`
	_, err = db.Exec(sqlStatement3)

	sqlStatement4 := `DELETE FROM aeroinventory.t_roll_fwd_job;`
	_, err = db.Exec(sqlStatement4)

	sqlStatement5 := `
	DELETE FROM aeroinventory.t_flight
	WHERE flight_number = $1
	OR flight_number = $2
	OR flight_number = $3
	OR flight_number = $4
	OR flight_number = $5;`
	_, err = db.Exec(sqlStatement5, "G9501","G9502","G9503","G9504","G9505")

	sqlStatement7 := `DELETE FROM aeroinventory.t_inv_block_request;`
	_, err = db.Exec(sqlStatement7)

	createNewBulkFlights()

	sqlStatement6 := `
	UPDATE aeroinventory.t_fcc_seg_alloc
	SET curtail_oversell = $1
	WHERE curtail_oversell IS NULL;`
	_, err = db.Exec(sqlStatement6, "CURTAIL")

	if err != nil {
		panic(err)
	}
	defer db.Close()

}


func getBulkFlightData(mtd *desc.MethodDescriptor, cd *runner.CallData)[]byte{
	bulkflightRQ := &aeroinventory.BulkFlightRQ{}
	bulkflightRQ.FromDate = fromDateG
	bulkflightRQ.ToDate = toDateG
	bulkflightRQ.FromFlightNumber = fromFlightNumberG
	bulkflightRQ.ToFlightNumber = toFlightNumberG

	binData, err := proto.Marshal(bulkflightRQ)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func createNewBulkFlights(){

	report, err := runner.Run(
		"aeroinventory.loadtesting.LoadTestingService/BulkFlightCreation",
		"localhost:6566",
		runner.WithProtoFile("/home/chirantha/codebases/goProj/goProtos/proto/load_testing.proto", []string{}),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(1)),
		runner.WithBinaryDataFunc(getBulkFlightData),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}
	printer.Print("pretty")
}



